// Fill out your copyright notice in the Description page of Project Settings.

#include "LD37.h"
#include "RWTextFile.h"


bool URWTextFile::LoadTxt(FString FileName, FString& LoadedText)
{
	return FFileHelper::LoadFileToString(LoadedText, *(FPaths::GameDir() + FileName));
}

bool URWTextFile::SaveTxt(FString SaveText, FString FileName)
{
	return FFileHelper::SaveStringToFile(SaveText, *(FPaths::GameDir() + FileName));
}

